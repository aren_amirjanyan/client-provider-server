var securityAPI = require('../helpers/security-api');
var _           = require('lodash');

module.exports = function (request, response, next) {
    response.deny = function (from) {
        this.status(403).json({statusCode: 403, message: from})
    };

    var token = request.headers['authorization'].replace('Bearer ','');

    if (token) {
        securityAPI.validateToken(token)
            .then(function (data) {
                response.setHeader('Authorization', 'Bearer '+ data.token);
                next();
            })
            .catch(function (reason) {
                response.deny(reason);
            })
    } else {
        return response.json({data : [], success : false, error : true, message :'Access denied'});
        next();
    }
};