module.exports = function (app) {
    app.use(function (error, request, response, next) {
        response.status(500).json({data : [], success: false, error :true, message: 'Something went wrong.'})
    })
};