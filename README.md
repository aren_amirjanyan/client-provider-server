# NodeJS, Mongo, Express REST API #

The project is REST API with node js (Express), mongo 

### How run REST API ? ###

* Clone repository - [git clone https://aren_amirjanyan@bitbucket.org/aren_amirjanyan/client-provider-server.git](git clone https://aren_amirjanyan@bitbucket.org/aren_amirjanyan/client-provider-server.git)
* npm install
* check /configs/index.js and change with your credentials
* npm start

### Documentation of REST API Created by Swagger
[http://localhost:3000/api-docs/](http://localhost:3000/api-docs/)