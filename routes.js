var express             = require('express');
var clientCtrl          = require('./controllers/ClientController');
var providerCtrl        = require('./controllers/ProviderController');
var tokenCtrl           = require('./controllers/TokenController');
var API                 = require('./middleware/api');
var clientValidation = require('./validation/client');
var providerValidation = require('./validation/provider');
var router = express.Router();

/**
 * Clients REST API endpoints by using API Middleware
 */
router.get('/clients',        [API], clientCtrl.getAllClients);
router.get('/clients/:id',    [API], clientCtrl.getClientById);
router.post('/clients',       [ API,
                                clientValidation.checkClientEmail(),
                                clientValidation.checkEmailIsUnique(),
                                clientValidation.checkName(),
                                clientValidation.checkPhone()
                               ],clientCtrl.create);

router.put('/clients/:id',    [ API,
                                clientValidation.checkClientEmail(),
                                //clientValidation.checkEmailIsUnique(),
                                clientValidation.checkName(),
                                clientValidation.checkPhone()
                              ], clientCtrl.update);
router.delete('/clients/:id', [API], clientCtrl.delete);

/**
 * Provider REST API endpoints by using API Middleware
 */
router.get('/providers',        [API], providerCtrl.getAllProviders);
router.get('/providers/:id',    [API], providerCtrl.getProviderById);
router.post('/providers',      [ API,
                                 providerValidation.checkProviderName(),
                                 providerValidation.checkProviderNameIsUnique()
                               ], providerCtrl.create);
router.put('/providers/:id',   [ API,
                                 providerValidation.checkProviderName(),
                                 //providerValidation.checkProviderNameIsUnique()
                               ], providerCtrl.update);
router.delete('/providers/:id', [API], providerCtrl.delete);


/**
 * Get Token
 */
router.get('/getToken',tokenCtrl.get);


module.exports = router;