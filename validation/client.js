const { check }         = require('express-validator/check');
const { sanitizeParam } = require('express-validator/filter');
var Client              = require('../models/Client');
var _                   = require('lodash');

class ClientValidation {

    checkClientEmail(){
        return check('email')
            .isEmail()
            .normalizeEmail()
            .exists();
    }

    checkEmailIsUnique() {
        return check('email')
                .custom(value => {
                    return Client.find({ _id : {$ne: null}, email: value}).then(client => {
                        if (client.length > 0) {
                            return Promise.reject('Email address already in use!');
                        }
                    })
                })
    }
    checkName(){
        return check('name').not().isEmpty().withMessage('Client name can not be empty.');
    }
    checkPhone(){
        return check('phone')
            .not()
            .isEmpty().withMessage('Phone number can not be empty.')
            .isNumeric().withMessage('Phone number should be numeric only.');
    }
}
module.exports  = new ClientValidation();