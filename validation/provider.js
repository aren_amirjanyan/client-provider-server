const { check,param } = require('express-validator/check');
const { sanitizeParam } = require('express-validator/filter');
var Provider = require('../models/Provider');


var _           = require('lodash');

class ClientValidation {

    checkProviderName() {
        return check('name')
            .not()
            .isEmpty().withMessage('Provider name can not be empty.')
            .isLength({ min: 6 }).withMessage('Provider name should be at least 6 symbols.');
    }
    checkProviderNameIsUnique() {
        return check('name')
            .custom(value => {
                return Provider.find({ _id : {$ne: null}, name: value}).then(client => {
                        if (client.length > 0) {
                            return Promise.reject('Provider already exist!');
                        }
                })
            })
    }
}
module.exports  = new ClientValidation();