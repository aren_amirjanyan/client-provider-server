var config = {
    local: {
    mode: 'local',
        port: 5000,
        mongodb_url :'mongodb://mongoadmin:mongoadminapi@ds023694.mlab.com:23694',
        mongodb_name :'nodejs_mongo_api',
        jwt_secret:"AwOWQ4NGQwMTE0MjNkYzlmYjYiLCJuYW1lIjoib3JtZW4",
        session_life_time: 600
   },
    production: {
        mode: 'production',
        port: 3000,
        mongodb_url :'mongodb://localhost:27017',
        mongodb_name :'nodejs_mongo_api',
        jwt_secret:"AwOWQ4NGQwMTE0MjNkYzlmYjYiLCJuYW1lIjoib3JtZW4",
        session_life_time: 600
    },
    staging: {
        mode: 'staging',
        port: 4000,
        mongodb_url :'',
        mongodb_name :'',
        jwt_secret:"",
        session_life_time: 600
    }
}
module.exports = function(mode) {
    return config[mode || process.argv[2] || 'local'] || config.local;
}