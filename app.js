var express = require('express');

var config = require('./config')();


var app = express();

app.set('port',process.env.PORT || 3000);

var bodyParser = require('body-parser');

app.use(bodyParser.json());

require('./middleware')(app);

var routes = require('./routes');

require('./middleware/error')(app);

app.use('/api/v1',routes);

const swaggerUi = require('swagger-ui-express');
const swaggerDocument = require('./swagger.json');

var options = {
    swaggerOptions: {
        validatorUrl : null
    }
};

app.use('/api-docs', swaggerUi.serve, swaggerUi.setup(swaggerDocument, options));

app.listen(app.get('port'), function(){
    console.log('Express server listening on port ' + app.get('port'));
});

