var Client  = require('../models/Client');
var jwt     = require('jsonwebtoken');
var config  = require('../config')();
var _       = require('lodash');
// ...rest of the initial code omitted for simplicity.
const { check, validationResult } = require('express-validator/check');


class ClientController {

    getAllClients(request, response, next) {
        Client.find({}).populate({
            path: 'providers',
            // Get friends of providers - populate the 'providers' array for every client
            populate: { path: 'Provider' },
            select: 'name'
            }).exec(function (error, clients) {
            if (error){
                return response.json({data : error, success : false, error : true, message : error.message});
            }
            if (!clients) {
                response.json({data : [], success : false, error : true, message : 'Client not found'});
                next();
            } else {
                response.json({data : clients, success : true, error : false, message : ''});
                next();
            }
        });
    }
    getClientById(request, response, next){

        const errors = validationResult(request);

        if (!errors.isEmpty()) {
            return response.json({data : errors.array()[0].value, success : false, error : true, message : errors.array()[0].msg});
        }
        if (!request.params.id) {
            return response.json({data : [], success : false, error : true, message : "Unknown client."});
            next();
        }
        var id = request.params.id;

        Client.findById(id, function (error, client) {
            if (error){
                return response.json({data : error, success : false, error : true, message : error.message});
            }
            if (!client) {
                response.json({data : [], success : false, error : true, message : 'Client not found'});
                next();
            } else {
                response.json({data : client, success : true, error : false, message : ''});
                next();
            }
        });
    }

    create(request, response, next) {

        var newClient = new Client({
            name: request.body.name,
            email: request.body.email,
            phone: request.body.phone,
            providers: request.body.providers
        });

        const errors = validationResult(request);

        if (!errors.isEmpty()) {
            return response.json({data : errors.array()[0].value, success : false, error : true, message : errors.array()[0].msg});
        }

        newClient.save(function (error, client) {
            if (error){
                return response.json({data : error, success : false, error : true, message : error.message});
            }

            if (!client) {
                response.json({data : [], success : false, error : true, message : 'Client not found'});
                next();
            } else {
                response.json({data : client, success : true, error : false, message : 'The client successfully created!'});
                next();
            }
        });
    }
    update(request, response, next) {

        const errors = validationResult(request);

        if (!errors.isEmpty()) {
            return response.json({data : errors.array()[0].value, success : false, error : true, message : errors.array()[0].msg});
        }

        if (!request.params.id) {
            return response.json({data : [], success : false, error : true, message : "Unknown Client."});
            next();
        }
        var id = request.params.id;

        Client.findById(id, function (error, client) {
            if (error){
                return response.json({data : error, success : false, error : true, message : error.message});
            }

            client.name = request.body.name;
            client.email = request.body.email;
            client.phone = request.body.phone;
            client.providers = request.body.providers;

            client.save(function (error, client) {
                if (error){
                    return response.json({data : error, success : false, error : true, message : error.message});
                }
                if (!client) {
                    response.json({data : [], success : false, error : true, message : 'Client not found'});
                    next();
                } else {
                    response.json({data: client, success : true, message : 'The client successfully updated!'});
                    next();
                }
            });
        });
    }
    delete(request, response, next){
        if (!request.params.id) {
            return response.json({data : [], success : false, error : true, message : "Unknown client."});
            next();
        }
        var id = request.params.id;
        Client.findByIdAndRemove(id, {}, function (error, client) {
            if (error){
                return response.json({data : error, success : false, error : true, message : error.message});
            }
            if (!client) {
                response.json({data : [], success : false, error : true, message : 'Client not found'});
                next();
            } else {
                response.json({data : [], success : true, error : false, message : 'Client successfully deleted!'});
                next();
            }
        });
    }
}

module.exports = new ClientController();