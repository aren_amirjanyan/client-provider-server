var Client = require('../models/Client');
var Provider = require('../models/Provider');
var jwt      = require('jsonwebtoken');
var config   = require('../config')();
var _        = require('lodash');
// ...rest of the initial code omitted for simplicity.
const { check, validationResult } = require('express-validator/check');


class ProviderController {

    getAllProviders(request, response, next) {
        Provider.find(function (error, providers) {
            if (error){
                return response.json({data : error, success : false, error : true, message : error.message});
            }
            if (!providers) {
                response.json({data : [], success : false, error : true, message : 'Provider not found'});
                next();
            } else {
                response.json({data : providers, success : true, error : false, message : ''});
                next();
            }
        });
    }
    getProviderById(request, response, next){
        if (!request.params.id) {
            return response.json({data : [], success : false, error : true, message : "Unknown Provider."});
            next();
        }
        var id = request.params.id;
        Provider.findById(id, function (error, provider) {
            if (error){
                return response.json({data : error, success : false, error : true, message : error.message});
            }
            if (!provider) {
                response.json({data : [], success : false, error : true, message : 'Provider not found'});
                next();
            } else {
                response.json({data : provider, success : true, error : false, message : ''});
                next();
            }
        });
    }

    create(request, response, next) {

        var newProvider = new Provider({
            name: request.body.name
        });
        const errors = validationResult(request);

        if (!errors.isEmpty()) {
            return response.status(200).json({data : errors.array()[0].value, success : false, error : true, message : errors.array()[0].msg});
        }
        newProvider.save(function (error, provider) {

            if (error){
                return response.json({data : error, success : false, error : true, message : error.message});
            }

            if (!provider) {
                response.json({data : [], success : false, error : true, message : 'Provider not found'});
                next();
            } else {
                response.json({data : provider, success : true, error : false, message : 'The Provider successfully created!'});
                next();
            }
        });
    }
    update(request, response, next) {

        const errors = validationResult(request);

        if (!errors.isEmpty()) {
            return response.json({data : errors.array()[0].value, success : false, error : true, message : errors.array()[0].msg});
        }

        if (!request.params.id) {
            return response.json({data : [], success : false, error : true, message : "Unknown Provider."});
            next();
        }
        var id = request.params.id;

        Provider.findById(id, function (error, provider) {
            if (error){
                return response.json({data : error, success : false, error : true, message : error.message});
            }

            provider.name = request.body.name;

            provider.save(function (error, provider) {
                if (error){
                    return response.json({data : error, success : false, error : true, message : error.message});
                }
                if (!provider) {
                    response.json({data : [], success : false, error : true, message : 'Provider not found'});
                    next();
                } else {
                    response.json({data: provider, success : true, message : 'The Provider successfully updated!'});
                    next();
                }
            });
        });
    }
    delete(request, response, next){
        if (!request.params.id) {
            return response.json({data : [], success : false, error : true, message : "Unknown Provider."});
            next();
        }
        var id = request.params.id;
        Provider.findByIdAndRemove(id, function (error, provider) {
            if (error){
                return response.json({data : error, success : false, error : true, message : error.message});
            }
            if (!provider) {
                response.json({data : [], success : false, error : true, message : 'Provider not found'});
                next();
            } else {
                Client.update(
                    { "providers": id },
                    { "$pull": { "providers": id} },
                    { multi: true },
                    function (error, client){
                        if (error){
                            return response.json({data : error, success : false, error : true, message : error.message});
                        }
                        if (!client) {
                            response.json({data : [], success : false, error : true, message : 'Provider not found'});
                            next();
                        } else {
                            response.json({data : [], success : true, error : false, message : 'Provider successfully deleted!'});
                            next();
                        }
                    }
                );
           }
        });
    }
}

module.exports = new ProviderController();