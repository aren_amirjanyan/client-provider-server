var jwt     = require('jsonwebtoken');
var config  = require('../config')();
var _       = require('lodash');


class TokenController {

    get(request, response, next) {

        var _application = JSON.parse(JSON.stringify(config));

        var token = jwt.sign(_.omit(_application, 'jwt_secret'), config.jwt_secret, { expiresIn: config.session_life_time });

        response.setHeader('Authorization', 'Bearer '+ token);

        response.send({data: [], success : true, error : false, token : token});
        next();

    }
}

module.exports = new TokenController();