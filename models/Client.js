var mongoose = require('./connection');
var ObjectId = mongoose.Schema.ObjectId;
var clientSchema = new mongoose.Schema({
    id: ObjectId,
    name: {
        type: String,
        required: true
    },
    email: {
        type: String,
        unique: true,
        required: true
    },
    phone: {
        type: String,
        required: true
    },
    providers :  [{ type: ObjectId, ref: 'Provider' }],
    created: Date,
});

var Client = mongoose.model('Client', clientSchema);
module.exports = Client;