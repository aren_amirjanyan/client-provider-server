var mongoose = require('mongoose');

var config = require('../config')();

mongoose.Promise = global.Promise;

mongoose.connect(config.mongodb_url + '/' + config.mongodb_name,{useMongoClient: true }, function(err){
    if(err){
        console.log(err);
    }
});

module.exports = mongoose;