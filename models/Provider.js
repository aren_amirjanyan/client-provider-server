var mongoose = require('./connection');
var ObjectId = mongoose.Schema.ObjectId;
var providerSchema = new mongoose.Schema({
    id: ObjectId,
    name: {
        type: String,
        unique: true,
        required: true
    },
    created: Date
});

var Provider = mongoose.model('Provider', providerSchema);
module.exports = Provider;